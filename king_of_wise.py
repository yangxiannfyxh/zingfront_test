# coding: utf-8
from __future__ import unicode_literals, absolute_import, print_function

"""
王者农药新模式——“智慧王者”，ᨀ供5个英雄，假设各自血量和攻击力如下：
梦琪：10000血，100攻
程咬金：5000血，200攻
亚瑟：2000血，500攻
铠：1000血，1000攻
狄仁杰：100血，2000攻
地图被画成一张 M x N 的棋盘，左上角为英雄的出生地，右下角则为敌方基地。所有的格子中，要么是“敌方水晶塔”，要
么是“回血包”。其中，敌方水晶塔会对英雄造成X点伤害，每个水晶塔有自己独立的伤害值，而加血卷轴可以恢复英雄的血
量。当英雄的血量小于等于0时，英雄死亡，游戏结束。英雄每次只能向下或向右移动一个格子，直到达到敌方基地的格子，
摧毁基地取得胜利。
如果给定一张随机生成的地图如下，其中，负数代表敌方水晶塔的伤害值，正数代表恢复血量值：
问：
1、选择哪个英雄，既可以保证胜利，又能ᨀ供最大攻击力？请给出算法，在任意随机地图下都有效。亦即，地图随机生成，
针对每一种随机地图，算法都可以给出英雄选择。
2、如上图给定的地图，应该选择哪一个英雄？
3、请给出以上算法所计算的最优路线。
"""
"""
   |   |   
---a---b---
   |   |   
---c---d---
   |   |   

1.假设任意英雄到达地图任意一点之后的血量最大增益（或者最小损失）值为m，
2.如上所示a、b、c、d四个位置要到达点d必须经过b、c中的一点，则可比较b、c处的m，确定如何到达点d，
3.以此种方法可以得到到达敌方水晶时的血量最大增益（或者最小损失）值，也就可以反推出其路径，
4.再根据该路径上的血量最大损失值，过滤掉途中会死亡的英雄，
5.最后从所有剩余英雄中找到攻击力最大的英雄
"""
LEFT = "right"
UP = "down"


class Location():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.pre = None
        self.value = 0

    def __repr__(self):
        return str(self.value)


"""
到达右下角的路径和血量变化值
"""


def blood(left, up, current):
    value = left.value
    pre = LEFT
    if left.value < up.value:
        pre = UP
        value = up.value
    return value + current, pre


def blood_map(m, n, mmap):
    bm = [[Location(j, i) for i in range(0, n)] for j in range(0, m)]
    bm[0][0].value = mmap[0][0]
    for i in range(1, n):
        bm[0][i].value = bm[0][i - 1].value + mmap[0][i]
        bm[0][i].pre = LEFT
    for j in range(1, m):
        bm[j][0].value = bm[j - 1][0].value + mmap[j][0]
        bm[j][0].pre = UP
    for j in range(1, m):
        for i in range(1, n):
            bm[j][i].value, bm[j][i].pre = blood(bm[j][i - 1], bm[j - 1][i], mmap[j][i])
    bm[m - 1][n - 1].value = bm[m - 1][n - 1].value - mmap[m - 1][n - 1]
    return bm


def wise_path(m, n, bm):
    l = []
    x = m - 1
    y = n - 1
    low = bm[x][y].value
    while x > 0 or y > 0:
        if low > bm[x][y].value:
            low = bm[x][y].value
            # print(low)
        loc = bm[x][y]
        if loc.pre == LEFT:
            x = x - 1
        else:
            y = y - 1
        l.append(loc.pre)
    length = len(l)
    return [l[length - i - 1] for i in range(0, length)], low


def hero_filter(heros, low):
    h = []
    for hero in heros:
        if hero[1] + low > 0:
            h.append(hero)
    return h


def att_hero(heros):
    a = 0
    if len(heros) > 0:
        for i in range(0, len(heros)):
            if heros[a][2] < heros[i][2]:
                a = i
        return heros[a]
    return None


def wise(m, n, mmap, heros):
    bm = blood_map(m, n, mmap)
    l, low = wise_path(m, n, bm)
    h = hero_filter(heros, low)
    hero = att_hero(h)
    return hero, l


if __name__ == '__main__':
    mmap = [[133, -523, -558, 846, -907, -1224, -1346, 787, -411, -1826],
            [-1478, -853, -1401, 341, -26, 759, -444, 174, -1594, -2000],
            [861, -584, 670, 696, 676, -1674, -1737, -1407, -484, 248],
            [458, -1669, -419, -382, -895, 732, -1278, -1802, -527, 862],
            [-1297, 544, -1943, 563, -380, -1268, 266, -1309, -1946, 85],
            [-1981, -1631, -168, 741, -211, -1070, -1873, -554, 243, -901],
            [849, 971, -21, -1111, 463, 944, -124, -1414, -1463, -1287],
            [70, -1884, -1159, -73, 555, -426, -190, -1750, -1028, -188],
            [-1220, -1654, -931, -1100, -433, -1643, -1281, -455, 904, -126],
            [-1494, -632, 243, 90, 993, 322, 32, -388, -225, 952]]
    heros = [["梦琪", 10000, 100],
             ["程咬金", 5000, 200],
             ["亚瑟", 2000, 500],
             ["铠", 1000, 1000],
             ["狄仁杰", 100, 2000]]
    rr = wise(9, 10, mmap, heros)
    print(rr[0][0])
    print(rr[1])
    # print(l)
