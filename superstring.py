# coding: utf-8
from __future__ import unicode_literals, absolute_import, print_function
"""
题目1：
我们定义一个字符串的超集为：
若字符串A包含字符串B中的所有字符，且字符串A中某一字符 a的数量不小于a在字符串B中的数量，那么A为B的“超集”。
例如：若字符串A为 "abbccdd"，字符串B为 "abcdd"，那么A是B的“超集”。若A为"abbccd"，字符串B为"abcdd"，那么A不是B的“超集”。
现给定字符串A、B，判断A是否是B的“超集”
"""
"""
1.统计A、B中各个字符出现的次数，
2.验证是否B中任意字符出现次数都小于在A中出现次数
"""
"""
假设所有字符集合为a-z,则取数组长度为字符集合大小（也就是23），其中每一个元素代表对应字符出现的次数
"""
max_size = 23

"""
设a长度为m，b长度为n
则时间复杂度为：m+n+23
空间复杂度为：23×2
"""
def count(s):
    c = [0 for i in range(max_size)]
    for i in s:
        j = ord(i) - ord('a')
        c[j] = c[j] + 1
    return c


def comp(a, b):
    for i in range(max_size):
        if a[i] < b[i]:
            return False
    return True


if __name__ == '__main__':
    a = "abbccdd"
    b = "abcdd"
    r = comp(count(a), count(b))
    print(a, "is the super set of", b, "?", r)
    a = "abbccd"
    b = "abcdd"
    r = comp(count(a), count(b))
    print(a, "is the super set of", b, "?",r)
